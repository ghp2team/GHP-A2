//================================================================================
//                  3D Sonic Games Development Kit (SonicGDK)
//                          by  Javier "Xaklse" Osset
//
//  Read SGDKGameInfo.uc file for details about permission to use this software.
//================================================================================
// SGDK Actor > Actor
//
// It's just a container for all SonicGDK objects whose parent class is Actor.
//================================================================================
class SGDKActor extends Actor
    abstract
    ClassGroup(SGDK,Visible);


defaultproperties
{
}
